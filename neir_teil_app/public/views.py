from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='login')
def homePublic(request):
  return render(request, 'homePublic.html')


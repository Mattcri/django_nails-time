"""neir_teil_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from auth_user.views import signIn, signUp, signUpAdmin, logOutUser, homeAdmin, editUserProfile
from public.views import homePublic

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', signIn, name='login'),
    path('logout/', logOutUser, name='logout'),
    path('register/', signUp, name='register'),
    path('', homePublic, name='home_public'),
    path('owner/', homeAdmin, name='home_admin'),
    path('owner/create-profile', signUpAdmin, name='create_admin'),
    path('edit-profile/', editUserProfile, name='edit_profile')
]

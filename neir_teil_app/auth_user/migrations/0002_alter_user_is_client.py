# Generated by Django 4.1.4 on 2022-12-15 08:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth_user', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='is_client',
            field=models.BooleanField(default=False, verbose_name='Is client'),
        ),
    ]

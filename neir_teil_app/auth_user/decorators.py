from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect

def admin_permission(view_func):
  def wrapper_func(request, *args, **kwargs):
    print('perfil:', request.user.is_admin)
    if request.user.is_admin:
      return view_func(request, *args, **kwargs)
    else:
      raise PermissionDenied
  return wrapper_func

def public_permission(view_func):
  def wrapper_func(request, *args, **kwargs):
    print('perfil:', request.user.is_client)
    if request.user.is_client:
      return view_func(request, *args, **kwargs)
    else:
      raise PermissionDenied
  return wrapper_func

def unauthenticated_user(view_func):
  def wrapper_func(request, *args, **kwargs):
    if request.user.is_authenticated:
      if request.user.is_admin:
        return redirect('home_admin')
      elif request.user.is_client:
        return redirect('home_public')

    else:
      return view_func(request, *args, **kwargs)
  return wrapper_func

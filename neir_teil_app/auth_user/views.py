from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .forms import SignUpForm, SignInForm, SignUpAdminForm, EditProfileForm
from .decorators import admin_permission, public_permission, unauthenticated_user
from .models import User

# Create your views here.

@unauthenticated_user
def signIn(request):
  form = SignInForm(request.POST or None)
  msg = None
  if request.method == 'POST':
    if form.is_valid():
      username = form.cleaned_data.get('username')
      password = form.cleaned_data.get('password')
      user = authenticate(username=username, password=password)
      if user is not None and user.is_admin:
        login(request, user)
        return redirect('home_admin')
      elif user is not None and user.is_client:
        login(request, user)
        return redirect('home_public')
      
      else:
        msg = 'Credenciales Invalidas'
    else:
      msg = 'Error en la validación de tus datos'
    
  return render(request, 'signIn.html', { 'form': form, 'msg': msg })

@unauthenticated_user
def signUp(request):
  msg = None
  if request.method == 'POST':
    form = SignUpForm(request.POST)
    if form.is_valid():
      #obj = instancia del objeto User
      objUser = form.save(commit=False)
      objUser.is_client = True
      objUser.save()
      return redirect('login')
  
  else:
    form = SignUpForm()

  context = { 'form': form, 'msg': msg }
  return render(request, 'signUp.html', context)

@login_required(login_url='login')
@admin_permission
def signUpAdmin(request):
  msg = None
  if request.method == 'POST':
    form = SignUpAdminForm(request.POST)
    if form.is_valid():
      objUser = form.save(commit=False)
      objUser.is_admin = True
      objUser.save()
      return redirect('home_admin')
  
  else:
    form = SignUpAdminForm()

  context = { 'form': form, 'msg': msg }
  return render(request, 'signUpAdmin.html', context)

def logOutUser(request):
  logout(request)
  return redirect('login')

@login_required(login_url='login')
@admin_permission
def homeAdmin(request):
  return render(request, 'homeAdmin.html')

def editUserProfile(request):
  msg = None
  # user = User.objects.get(id=request.user.id)
  # print(user.id)
  form = EditProfileForm(instance=request.user)
  if request.method == 'POST':
    form = EditProfileForm(request.POST, instance=request.user)
    if form.is_valid():
      form.save()
      return redirect('login')
    else:
      msg = 'Datos no validos'


  context = { 'form': form, 'msg': msg }
  return render(request, 'EditProfile.html', context)
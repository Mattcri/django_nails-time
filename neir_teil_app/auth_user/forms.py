from django.forms import ModelForm, TextInput, EmailInput, PasswordInput
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms
from .models import User

class SignUpForm(UserCreationForm):
  username = forms.CharField(
    widget = forms.TextInput( attrs = { 
      "class": "form-control",
      "id": "username"
    } )
  )
  email = forms.CharField(
    widget = forms.EmailInput( attrs = { 
      "class": "form-control",
      "id": "email"
    } )
  )
  first_name = forms.CharField(
    widget = forms.TextInput( attrs = {
      "class": "form-control",
      "id": "first_name"
    } )
  )
  last_name = forms.CharField(
    widget = forms.TextInput( attrs = {
      "class": "form-control",
      "id": "last_name"
    } )
  )
  password1 = forms.CharField(
    widget = forms.PasswordInput( attrs = { 
      "class": "form-control",
      "id": "password1"
    } )
  )
  password2 = forms.CharField(
    widget = forms.PasswordInput( attrs = { 
      "class": "form-control",
      "id": "password2"
    } )
  )

  class Meta:
    model = User
    fields = ('username', 'email', 'first_name', 'last_name','password1', 'password2', 'is_admin', 'is_client')


class SignUpAdminForm(UserCreationForm):
  username = forms.CharField(
    widget = forms.TextInput( attrs = { 
      "class": "form-control",
      "id": "username"
    } )
  )
  email = forms.CharField(
    widget = forms.EmailInput( attrs = { 
      "class": "form-control",
      "id": "email"
    } )
  )
  first_name = forms.CharField(
    widget = forms.TextInput( attrs = {
      "class": "form-control",
      "id": "first_name"
    } )
  )
  last_name = forms.CharField(
    widget = forms.TextInput( attrs = {
      "class": "form-control",
      "id": "last_name"
    } )
  )
  password1 = forms.CharField(
    widget = forms.PasswordInput( attrs = { 
      "class": "form-control",
      "id": "password1"
    } )
  )
  password2 = forms.CharField(
    widget = forms.PasswordInput( attrs = { 
      "class": "form-control",
      "id": "password2"
    } )
  )

  class Meta:
    model = User
    fields = ('username', 'email', 'first_name', 'last_name','password1', 'password2', 'is_admin', 'is_client')

class SignInForm(forms.Form):
  username = forms.CharField(
    widget = forms.TextInput( attrs = { 
      "class": "form-control",
      "id": "username"
    } )
  )
  password = forms.CharField(
    widget = forms.PasswordInput( attrs = { 
      "class": "form-control",
      "id": "password"
    } )
  )

class EditProfileForm(UserChangeForm):
  class Meta:
    model = User
    fields = ('username', 'email', 'first_name', 'last_name', 'password')
    widgets = {
      'username': TextInput(attrs={
        "class": "form-control",
        "id": "username"
      }),
      'email': EmailInput(attrs={
        "class": "form-control",
        "id": "email"
      }),
      'first_name': TextInput(attrs={
        "class": "form-control",
        "id": "first_name"
      }),
      'last_name': TextInput(attrs={
        "class": "form-control",
        "id": "last_name"
      })
    }

  # username = forms.CharField(
  #   widget = forms.TextInput( attrs = { 
  #     "class": "form-control",
  #     "id": "username"
  #   } )
  # )
  # email = forms.CharField(
  #   widget = forms.EmailInput( attrs = { 
  #     "class": "form-control",
  #     "id": "email"
  #   } )
  # )
  # first_name = forms.CharField(
  #   widget = forms.TextInput( attrs = {
  #     "class": "form-control",
  #     "id": "first_name"
  #   } )
  # )
  # last_name = forms.CharField(
  #   widget = forms.TextInput( attrs = {
  #     "class": "form-control",
  #     "id": "last_name"
  #   } )
  # )
  # password = forms.CharField(
  #   widget = forms.PasswordInput( attrs = { 
  #     "class": "form-control",
  #     "id": "password"
  #   } )
  # )



